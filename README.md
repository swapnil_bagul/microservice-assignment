To check the Response from each service,Please find out the snapshot in Images folder.
There are 3 images.

All the services are invoked through API GATEWAY only.

1) CREATE NEW ACCOUNT
url: http://localhost:8080/account-service/create-new-account (POST Request)

2) GET ALL TRANSACTIONS
url: http://localhost:8080/transaction-service/get-all-transactions/{accountNmber} (GET Request)

3) MAKE NEW TRANSACTION
url: http://localhost:8080/transaction-service/make-transaction?from_account={from_account}&to_account={to_account}&ammount={ammount}  (PUT Request)