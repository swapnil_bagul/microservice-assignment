package com.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

@Entity
@Table(name = "bank_account")
public class BankAccount {

	@Id
	@Column(name = "account_number")
	private Long accountNumber;

	@Column
	@NotEmpty
	private String name;
	@Column
	@NotEmpty
	private String password;

	@NotEmpty
	@Email
	@Column(name = "email_id")
	private String emailId;

	@NotNull
	@Column(name = "mobile_number", length = 10)
	private Long mobileNumber;

	@NotEmpty
	@Column(name = "dob")
	private String dob;

	@NotEmpty
	@Size(min = 10, message = "pan number should be of 10 characters")
	@Column(name = "pan_card")
	private String panNumber;

	@NotNull
	@Column(name = "adhar_card")
	private Long aadharNumber;

	@NotEmpty
	@Column
	private String city;

	@Column
	@Min(value = 500, message = "Minimum 500 ammount needed while opening the account")
	private double ammount;

	public BankAccount() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Long getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(Long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public Long getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(Long aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public double getAmmount() {
		return ammount;
	}

	public void setAmmount(double ammount) {
		this.ammount = ammount;
	}

	@Override
	public String toString() {
		return "BankAccount [name=" + name + ", accountNumber=" + accountNumber + ", password=" + password
				+ ", emailId=" + emailId + ", mobileNumber=" + mobileNumber + ", DOB=" + dob + ", panNumber="
				+ panNumber + ", aadharNumber=" + aadharNumber + ", address=" + city + "]";
	}

}
