package com.app.service;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.app.entity.BankAccount;

@Service
public interface AccountService {

	Map createNewAccount(BankAccount account);

	double checkAccountBalanceService(Long accountNumber);

	double updateAccBalanceService(Long accountNumber, double ammount);
}
