package com.app.service;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.AccountDao;
import com.app.entity.BankAccount;
import com.app.exception.AccountException;

@Service
public class AccountServiceImpl implements AccountService {

	private static final Logger log = LoggerFactory.getLogger(AccountServiceImpl.class);

	@Autowired
	private AccountDao accountDao;

	@Override
	public Map createNewAccount(BankAccount account) {
		Map returnMap = new LinkedHashMap<>();
		String message = "";
		account.setAccountNumber(new Random().nextLong(10000000000L));
		try {
			BankAccount createdBankAccount = accountDao.save(account);
			message = "Bank Account created (Account Number ".toUpperCase() + createdBankAccount.getAccountNumber()
					+ ")";
			returnMap.put("MESSAGE", "Bank Account created (Account Number ".toUpperCase()
					+ createdBankAccount.getAccountNumber() + ")");
			returnMap.put("ACCOUNT INFORMATION", createdBankAccount);
			return returnMap;
		} catch (Exception e) {
			throw new AccountException(e.getMessage());
		}
	}

	@Override
	public double checkAccountBalanceService(Long accountNumber) {
		BankAccount account = null;
		try {
			account = accountDao.findByaccountNumber(accountNumber);
		} catch (Exception e) {
			throw new AccountException(e.getMessage());
		}
		if (account == null) {
			throw new AccountException("The account doesn't exist.".toUpperCase());
		}
		return account.getAmmount();
	}

	@Override
	public double updateAccBalanceService(Long accountNumber, double ammount) {
		BankAccount account = null;
		try {
			account = accountDao.findByaccountNumber(accountNumber);
			account.setAmmount(ammount);
			accountDao.save(account);
		} catch (Exception e) {
			throw new AccountException(e.getMessage());
		}
		if (account == null) {
			throw new AccountException("The account doesn't exist.".toUpperCase());
		}

		return account.getAmmount();
	}

}
