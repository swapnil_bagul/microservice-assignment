package com.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.entity.BankAccount;

@Repository
public interface AccountDao extends JpaRepository<BankAccount, Integer> {

	BankAccount findByaccountNumber(Long accountNumber);
}
