package com.app.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.entity.BankAccount;
import com.app.service.AccountService;

@RestController
@RequestMapping("/account-service")
public class AccountController {

	@Autowired
	private AccountService accountService;

	@PostMapping("create-new-account")
	public ResponseEntity<Map> createNewAccount(@Valid @RequestBody BankAccount account) {
		return new ResponseEntity<>(accountService.createNewAccount(account), HttpStatus.OK);
	}

	@GetMapping("check-account-balance/{accountNumber}")
	public ResponseEntity<Double> checkAccountBalance(@PathVariable Long accountNumber) {
		return new ResponseEntity<>(accountService.checkAccountBalanceService(accountNumber), HttpStatus.OK);
	}

	@PutMapping("update-account-balance")
	public ResponseEntity<Double> updateAccountBalanceForTransactions(@RequestParam Long accountNumber,
			@RequestParam double ammount) {
		return new ResponseEntity<>(accountService.updateAccBalanceService(accountNumber, ammount), HttpStatus.OK);

	}

}


//http://localhost:8088/account-service/create-new-account
//http://localhost:8088/account-service/check-account-balance/{accountNumber}
