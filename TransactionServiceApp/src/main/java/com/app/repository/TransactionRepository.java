package com.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.app.entity.Transactions;

public interface TransactionRepository extends CrudRepository<Transactions, Integer> {
	List<Transactions> findAllByaccountNumber(Long account);
}
