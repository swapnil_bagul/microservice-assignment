package com.app.exception;

public class TransactionsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TransactionsException(String msg) {
		super(msg);
	}
}
