package com.app.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.entity.Transactions;
import com.app.service.TransactionService;

///http://localhost:8089/transaction-service/get-all-transactions/1762120354
//http://localhost:8089/transaction-service/make-transaction?from_account=1762120354&to_account=1762120354&ammount=200

@RestController
@RequestMapping("transaction-service")
public class TransactionController {

	@Autowired
	private TransactionService txnService;

	@GetMapping(value = "get-all-transactions/{account}")
	public ResponseEntity<List<Transactions>> getAllTransactionsForAccount(@PathVariable("account") Long account) {
		return new ResponseEntity<>(txnService.getAllTransactionsForAccount(account), HttpStatus.OK);
	}

	@PostMapping("make-transaction")
	public ResponseEntity<Map> makeTransaction(@RequestParam("from_account") Long fromAccountNumber,
			@RequestParam("to_account") Long toAccountNumber, @RequestParam("ammount") double ammount) {
		return new ResponseEntity<>(
				txnService.transferAmmountToOtherAccount(fromAccountNumber, toAccountNumber, ammount), HttpStatus.OK);
	}

}
