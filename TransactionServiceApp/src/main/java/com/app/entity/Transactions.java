package com.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transactions")
public class Transactions {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "transaction_id")
	private Integer transactionId;

	@Column(name = "transaction_date")
	private String trasactionDate;

	@Column(name = "trans_details")
	private String transDetails;

	@Column(name = "account_number")
	private Long accountNumber;

	public Transactions(Integer transactionId, String trasactionDate, String transDetails, Long accountNumber) {
		super();
		this.transactionId = transactionId;
		this.trasactionDate = trasactionDate;
		this.transDetails = transDetails;
		this.accountNumber = accountNumber;
	}

	public Transactions() {
		super();
	}

	public Integer getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}

	public String getTrasactionDate() {
		return trasactionDate;
	}

	public void setTrasactionDate(String trasactionDate) {
		this.trasactionDate = trasactionDate;
	}

	public Long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getTransDetails() {
		return transDetails;
	}

	public void setTransDetails(String transDetails) {
		this.transDetails = transDetails;
	}

}
