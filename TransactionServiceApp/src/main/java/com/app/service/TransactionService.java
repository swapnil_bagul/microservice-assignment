package com.app.service;

import java.util.List;
import java.util.Map;

import com.app.entity.Transactions;

public interface TransactionService {

	List<Transactions> getAllTransactionsForAccount(Long account);

	Map transferAmmountToOtherAccount(Long fromAccountNumber, Long toAccountNumber, double ammount);
}
