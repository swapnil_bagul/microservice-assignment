package com.app.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.app.entity.Transactions;
import com.app.exception.TransactionsException;
import com.app.repository.TransactionRepository;

@Service
@ConfigurationProperties
public class TransactionServiceImpl implements TransactionService {

	@Value("${GET_CURRENT_BALANCE}")
	private String GET_CURRENT_BALANCE;

	@Value("${UPDATE_ACCOUNT_BALANCE_AFTER_TRANSACTION}")
	private String UPDATE_ACCOUNT_BALANCE_AFTER_TRANSACTION;
	@Autowired
	private TransactionRepository transactionRepository;

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public List<Transactions> getAllTransactionsForAccount(Long account) {
		List<Transactions> txnList = null;
		try {
			txnList = transactionRepository.findAllByaccountNumber(account);
		} catch (Exception e) {
			throw new TransactionsException(e.getMessage());
		}
		return txnList;
	}

	@SuppressWarnings("all")
	@Transactional
	public Map transferAmmountToOtherAccount(Long fromAccountNumber, Long toAccountNumber, double ammount) {
		try {

			Map<String, Long> params = new HashMap<>();
			params.put("accountNumber", fromAccountNumber);
			double fromAccountBalance = restTemplate.getForObject(GET_CURRENT_BALANCE, Double.class, params);
			params.put("accountNumber", toAccountNumber);
			double toAccountBalance = restTemplate.getForObject(GET_CURRENT_BALANCE, Long.class, params);
			if (fromAccountBalance <= ammount) {
				throw new TransactionsException("You dont have enough balance to make the transfer");
			} else {
				Transactions txn1 = new Transactions();
				txn1.setAccountNumber(fromAccountNumber);
				txn1.setTrasactionDate(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
				double remainingBal = fromAccountBalance - ammount;

				Map updateParams = new HashMap<>();
				updateParams.put("accountNumber", fromAccountNumber);
				updateParams.put("ammount", remainingBal);
				restTemplate.put(UPDATE_ACCOUNT_BALANCE_AFTER_TRANSACTION, Double.class, updateParams);
				txn1.setTransDetails(ammount + " debited and transfer to " + toAccountNumber);
				txn1 = transactionRepository.save(txn1);
				Transactions txn2 = new Transactions();
				double updatedBal = toAccountBalance + ammount;
				txn2.setAccountNumber(toAccountNumber);
				txn2.setTrasactionDate(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));

				updateParams.put("accountNumber", toAccountNumber);
				updateParams.put("ammount", updatedBal);
				restTemplate.put(UPDATE_ACCOUNT_BALANCE_AFTER_TRANSACTION, Double.class, updateParams);
				txn2.setTransDetails(ammount + " credited from " + fromAccountNumber);
				txn2 = transactionRepository.save(txn2);
				Map returnMap = new LinkedHashMap<>();
				returnMap.put("MESSAGE", "TRANSACTION SUCCESSFUL");
				returnMap.put("DEBIT Transaction", txn1);
				returnMap.put("CREDIT Transaction", txn2);
				return returnMap;
			}
		} catch (Exception e) {
			throw new TransactionsException(e.getMessage());
		}
	}
}
